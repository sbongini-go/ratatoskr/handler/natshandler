package natshandler

import (
	"context"
	"os"
	"testing"

	"github.com/phayes/freeport"
	"github.com/rs/zerolog"
	"gitlab.alm.poste.it/go/ilog/ilog"
	"gitlab.com/sbongini-go/ratatoskr/core"
	"gitlab.com/sbongini-go/ratatoskr/core/handler/customhandler"
	"gitlab.com/sbongini-go/ratatoskr/source/natssource"
	"go.opentelemetry.io/otel/exporters/jaeger"
	"go.opentelemetry.io/otel/exporters/stdout/stdouttrace"
	"gotest.tools/assert"

	"github.com/nats-io/gnatsd/server"
	natsservertest "github.com/nats-io/nats-server/test"
	natslib "github.com/nats-io/nats.go"

	izerolog "gitlab.alm.poste.it/go/ilog/zerolog"

	"gitlab.com/sbongini-go/ratatoskr/core/model"
)

func startNatsTestServer() (*server.Server, error) {
	port, err := freeport.GetFreePort()
	if nil != err {
		return nil, err
	}

	opts := natsservertest.DefaultTestOptions
	opts.Port = port
	return natsservertest.RunServer(&opts), nil
}

func startCoreNatsSource(natsURL string, completeChan chan struct{}) core.Core {
	// Canale su cui arrivera una notifica quando il Core e' ready
	chanReady := make(chan struct{})

	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix

	//logger := stdlog.NewStdLog(stdlog.WithLevel(ilog.DEBUG))
	logger := izerolog.NewZeroLog(
		izerolog.WithLogger(zerolog.New(os.Stderr).With().Timestamp().Logger()),
		izerolog.WithLevel(ilog.DEBUG),
	)

	// Istanzio gli exporter per OpenTelemetry
	jaegerExporter, _ := jaeger.New(jaeger.WithCollectorEndpoint(jaeger.WithEndpoint("http://jaeger:14268/api/traces")))
	stdoutExporter, _ := stdouttrace.New()

	coreObj := core.NewCore(
		core.WithServiceName("ratatoskr"),
		core.WithServiceVersion("v0.1.0"),
		core.WithServiceNamespace("ns-demo"),
		core.WithServiceAttribute("environment", "demo"),
		core.WithLogger(logger),
		core.WithSpanExporter(stdoutExporter),
		core.WithSpanExporter(jaegerExporter),
		core.WithChanReady(chanReady),
		core.WithSource("nats-source", natssource.New(natsURL,
			natssource.WithQueueSubscriber(natssource.Builder("input", "natsHandler").BuildQueueSubscriber("queue-name")),
			natssource.WithQueueSubscriber(natssource.Builder("outout", "end").BuildQueueSubscriber("queue-name")),
		)),
		core.WithHandler("natsHandler", New(natsURL, "outout")),
		core.WithHandler("end", customhandler.New(func(ctx context.Context, bridge *core.Bridge, event model.Event) (model.Event, error) {
			completeChan <- struct{}{} // Notifico il completamento del test
			return event, nil
		})),
	)

	go coreObj.Start()

	<-chanReady // Dopo questa riga il Core sara ready

	return coreObj
}

func TestNats(t *testing.T) {
	// Avvio il server Nats di test
	natsServer, err := startNatsTestServer()
	assert.NilError(t, err)
	defer natsServer.Shutdown()

	// URL da usare per connettersi al NATS Server
	natsURL := natsServer.Addr().String()

	//natsURL = "nats://nats-0:4222" // DECOMMENTARE per usare istanza remota devcontainer

	completeChan := make(chan struct{}) // Canale su cui arrivera la notifica di completamento
	coreObj := startCoreNatsSource(natsURL, completeChan)
	defer coreObj.Stop()

	// Produco messaggi su Nats
	natsOpts := natslib.Options{
		Name: "unicorn-producer",
		Url:  natsURL,
	}

	natsConnection, err := natsOpts.Connect()
	assert.NilError(t, err)

	t.Run("publish", func(t *testing.T) {
		// Publico un messaggio su Nats
		err = natsConnection.Publish("input", []byte(`{"name":"amber"}`))
		assert.NilError(t, err)

		<-completeChan
	})
}
