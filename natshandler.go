package natshandler

import (
	"context"
	"fmt"
	"time"

	"github.com/nats-io/nats.go"
	"gitlab.com/sbongini-go/mapstructureplus"
	"gitlab.com/sbongini-go/ratatoskr/core"
	"gitlab.com/sbongini-go/ratatoskr/core/handler"
	"gitlab.com/sbongini-go/ratatoskr/core/model"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/codes"
	"go.opentelemetry.io/otel/trace"
)

func NewFromInterface(data interface{}) (core.Handler, error) {
	h := newDefault()
	err := mapstructureplus.PrivateDecode(data, &h)
	return &h, err
}

func (h natsHandler) MarshalYAML() (interface{}, error) {
	return struct {
		Name               string
		Type               string
		URL                string
		Subject            string
		Reply              bool
		Pedantic           bool
		Timeout            time.Duration
		PingInterval       time.Duration
		MaxPingsOut        int
		NoEcho             bool
		Verbose            bool
		AllowReconnect     bool
		MaxReconnect       int
		ReconnectWait      time.Duration
		ReconnectJitter    time.Duration
		ReconnectJitterTLS time.Duration
		ReconnectBufSize   int
		DrainTimeout       time.Duration
		SubChanLen         int
	}{
		Name:               h.Name(),
		Type:               h.Type(),
		URL:                h.url,
		Subject:            h.subject,
		Reply:              h.reply,
		Pedantic:           h.pedantic,
		Timeout:            h.timeout,
		PingInterval:       h.pingInterval,
		MaxPingsOut:        h.maxPingsOut,
		NoEcho:             h.noEcho,
		Verbose:            h.verbose,
		AllowReconnect:     h.allowReconnect,
		MaxReconnect:       h.maxReconnect,
		ReconnectWait:      h.reconnectWait,
		ReconnectJitter:    h.reconnectJitter,
		ReconnectJitterTLS: h.reconnectJitterTLS,
		ReconnectBufSize:   h.reconnectBufSize,
		DrainTimeout:       h.drainTimeout,
		SubChanLen:         h.subChanLen,
	}, nil
}

type natsHandler struct {
	// embedded type contente le parti comuni degli handlers
	handler.CommonHandler `mapstructure:",squash"`

	// Configurazioni per Nats
	url                string        `yaml:"url" mapstructure:"url"`
	pedantic           bool          `yaml:"pedantic" mapstructure:"pedantic"`
	timeout            time.Duration `yaml:"timeout" mapstructure:"timeout"`
	pingInterval       time.Duration `yaml:"pinginterval" mapstructure:"pinginterval"`
	maxPingsOut        int           `yaml:"maxpingsout" mapstructure:"maxpingsout"`
	noEcho             bool          `yaml:"noecho" mapstructure:"noecho"`
	verbose            bool          `yaml:"verbose" mapstructure:"verbose"`
	allowReconnect     bool          `yaml:"allowreconnect" mapstructure:"allowreconnect"`
	maxReconnect       int           `yaml:"maxreconnect" mapstructure:"maxreconnect"`
	reconnectWait      time.Duration `yaml:"reconnectwait" mapstructure:"reconnectwait"`
	reconnectJitter    time.Duration `yaml:"reconnectjitter" mapstructure:"reconnectjitter"`
	reconnectJitterTLS time.Duration `yaml:"reconnectjittertls" mapstructure:"reconnectjittertls"`
	reconnectBufSize   int           `yaml:"reconnectbufsize" mapstructure:"reconnectbufsize"`
	drainTimeout       time.Duration `yaml:"draintimeout" mapstructure:"draintimeout"`
	subChanLen         int           `yaml:"subchanlen" mapstructure:"subchanlen"`

	// subject Nats su cui produrre i messaggi
	subject string `yaml:"subject" mapstructure:"subject"`

	// reply indica se utilizzare la modalita Request-Reply
	reply bool `yaml:"reply" mapstructure:"reply"`

	// connection connessione al server Nats
	connection *nats.Conn `yaml:"-"`

	// natsOpts opzioni Nats per connetersi al server Nats
	natsOpts nats.Options `yaml:"-"`
}

// newDefault Crea un nuovo NatsHandler di default
func newDefault() natsHandler {
	defaultOps := nats.GetDefaultOptions()
	return natsHandler{
		// Configurazioni per Nats
		url:                defaultOps.Url,
		pedantic:           defaultOps.Pedantic,
		timeout:            defaultOps.Timeout,
		pingInterval:       defaultOps.PingInterval,
		maxPingsOut:        defaultOps.MaxPingsOut,
		noEcho:             defaultOps.NoEcho,
		verbose:            defaultOps.Verbose,
		allowReconnect:     defaultOps.AllowReconnect,
		maxReconnect:       defaultOps.MaxReconnect,
		reconnectWait:      defaultOps.ReconnectWait,
		reconnectJitter:    defaultOps.ReconnectJitter,
		reconnectJitterTLS: defaultOps.ReconnectJitterTLS,
		reconnectBufSize:   defaultOps.ReconnectBufSize,
		drainTimeout:       defaultOps.DrainTimeout,
		subChanLen:         defaultOps.SubChanLen,

		reply: false,
	}
}

// NatsHandler istanzia un nuovo NatsHandler
func New(url string, subject string, opts ...Option) core.Handler {
	h := newDefault()

	h.url = url         // Imposto l'URL per il server Nats
	h.subject = subject // Imposto il subject a cui inviare i messaggi

	// Applico gli option patterns
	for _, opt := range opts {
		opt(&h)
	}

	return &h
}

// Init funzione di inizializzazione del Handler
func (h *natsHandler) Init(ctx context.Context, initBag core.InitHandlerBag) error {
	h.SetLogger(initBag.Logger)

	h.natsOpts = nats.Options{
		Url:                h.url,
		Pedantic:           h.pedantic,
		Timeout:            h.timeout,
		PingInterval:       h.pingInterval,
		MaxPingsOut:        h.maxPingsOut,
		NoEcho:             h.noEcho,
		Verbose:            h.verbose,
		AllowReconnect:     h.allowReconnect,
		MaxReconnect:       h.maxReconnect,
		ReconnectWait:      h.reconnectWait,
		ReconnectJitter:    h.reconnectJitter,
		ReconnectJitterTLS: h.reconnectJitterTLS,
		ReconnectBufSize:   h.reconnectBufSize,
		DrainTimeout:       h.drainTimeout,
		SubChanLen:         h.subChanLen,
	}

	// Nome connesione, non e' obbligatoria ma fortemente consigliato per monitoraggio, ecc..
	h.natsOpts.Name = fmt.Sprintf("%s-%s", initBag.ServiceName, initBag.HandlerName)

	return nil
}

// Start avvia l'handler avviando il client Nats per produrre messaggi
func (h *natsHandler) Start(ctx context.Context) error {
	// Mi connetto al server NATS
	h.Logger().Info("connecting to Nats server ...")
	connection, err := h.natsOpts.Connect()
	if nil != err {
		return err
	}
	h.connection = connection
	h.Logger().Info("Nats server connected")

	return nil
}

// Execute esegue la logica del Handler
func (h natsHandler) Execute(ctx context.Context, bridge *core.Bridge, event model.Event) (model.Event, error) {
	span := trace.SpanFromContext(ctx)
	span.SetAttributes(
		attribute.String("subject", h.subject),
		attribute.Bool("request-reply", h.reply),
	)

	inputMsg := nats.Msg{
		Subject: h.subject,
		Header:  event.Metadata,
		Data:    event.Data,
	}

	// Se la modalita' Request-Reply e' abilitata la uso
	if h.reply {
		// Uso la modalita Request-Reply
		outputMsg, err := h.connection.RequestMsg(&inputMsg, time.Duration(1*time.Second))
		if nil != err {
			span.RecordError(err)
			span.SetStatus(codes.Error, err.Error())
			return model.Event{}, err
		}

		return model.Event{
			Metadata: outputMsg.Header,
			Data:     outputMsg.Data,
		}, nil
	}

	// Publico il messaggio su Nats
	err := h.connection.PublishMsg(&inputMsg)
	if nil != err {
		span.RecordError(err)
		span.SetStatus(codes.Error, err.Error())
		return model.Event{}, err
	}

	return model.Event{}, nil
}

// Stop arresta l'handler
func (h *natsHandler) Stop(ctx context.Context) error {
	if h.connection != nil {
		return h.connection.Drain()
	}
	return nil
}

// Type restituisce il tipo di handler
func (h *natsHandler) Type() string {
	return "nats"
}
