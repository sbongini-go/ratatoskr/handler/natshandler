package natshandler

import "time"

// Option tipo funzione che rappresenta un opzione funzionale per inizializzare il natsHandler
type Option func(*natsHandler)

// WithReply se a true l'handler Nats usera la modalita Request-Reply
func WithReply(reply bool) Option {
	return func(c *natsHandler) {
		c.reply = reply
	}
}

// WithPedantic opzione funzionale Abilitando il pedantic mode si abilitano controlli aggiuntivi sul protocollo
func WithPedantic(pedantic bool) Option {
	return func(c *natsHandler) {
		c.pedantic = pedantic
	}
}

// WithTimeout timeout di connessione
func WithTimeout(timeout time.Duration) Option {
	return func(c *natsHandler) {
		c.timeout = timeout
	}
}

// WithPingInterval intervallo di Ping nel protocollo ping/pong.
// Nel protocollo ping/pong la connessione viene chiusa dopo un numero di chiamate ping senza risposta,
// tra una chiamata ping e l'altra si attende un intervallo di tempo.
func WithPingInterval(pingInterval time.Duration) Option {
	return func(c *natsHandler) {
		c.pingInterval = pingInterval
	}
}

// WithMaxPingsOut numero di ping senza risposta pong ammessi
// Nel protocollo ping/pong la connessione viene chiusa dopo un numero di chiamate ping senza risposta,
// tra una chiamata ping e l'altra si attende un intervallo di tempo.
func WithMaxPingsOut(maxPingsOut int) Option {
	return func(c *natsHandler) {
		c.maxPingsOut = maxPingsOut
	}
}

// WithNoEcho se a true disabilita gli echo messages
func WithNoEcho(noEcho bool) Option {
	return func(c *natsHandler) {
		c.noEcho = noEcho
	}
}

// WithVerbose modalita verbosa sola per debug
func WithVerbose(verbose bool) Option {
	return func(c *natsHandler) {
		c.verbose = verbose
	}
}

// WithAllowReconnect indica se il client riprova a riconnetersi in automatico se viene disconesso per un qualsiasi motivo
func WithAllowReconnect(allowReconnect bool) Option {
	return func(c *natsHandler) {
		c.allowReconnect = allowReconnect
	}
}

// WithMaxReconnect imposta il numero massimo di tentativi di riconnesione a una istanza di server Nats,
// una volta terminati i tentativi, il server URL viene rimosso dalla lista,
// potra essere ri-inserito nella lista se dopo una connessione andata a buon fine e verra nuovamente indicato come server attivo
func WithMaxReconnect(maxReconnect int) Option {
	return func(c *natsHandler) {
		c.maxReconnect = maxReconnect
	}
}

// WithReconnectWait imposta il tempo da attendere prima di riprovare a riconnetersi a un server Nats
func WithReconnectWait(reconnectWait time.Duration) Option {
	return func(c *natsHandler) {
		c.reconnectWait = reconnectWait
	}
}

// WithReconnectJitter imposta un limite superiore per un ritardo casuale di riconnesione
func WithReconnectJitter(reconnectJitter time.Duration) Option {
	return func(c *natsHandler) {
		c.reconnectJitter = reconnectJitter
	}
}

// WithreConnectJitterTLS imposta un limite superiore per un ritardo casuale di riconnesione per le connessioni TLS
func WithreConnectJitterTLS(reconnectJitterTLS time.Duration) Option {
	return func(c *natsHandler) {
		c.reconnectJitterTLS = reconnectJitterTLS
	}
}

// WithreConnectBufSize dimensione del bufio di supporto durante la riconnessione.
// Una volta che questo è stato esaurito, le operazioni di pubblicazione restituiranno un errore.
func WithreConnectBufSize(reconnectBufSize int) Option {
	return func(c *natsHandler) {
		c.reconnectBufSize = reconnectBufSize
	}
}

// WithDrainTimeout timeout per completare l'operazione di Drain
func WithDrainTimeout(drainTimeout time.Duration) Option {
	return func(c *natsHandler) {
		c.drainTimeout = drainTimeout
	}
}

// WithSubChanLen dimensione del canale bufferizzato utilizzato tra il socket
func WithSubChanLen(subChanLen int) Option {
	return func(c *natsHandler) {
		c.subChanLen = subChanLen
	}
}
