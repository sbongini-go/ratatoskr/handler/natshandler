module gitlab.com/sbongini-go/ratatoskr/handler/natshandler

go 1.20

require (
	github.com/nats-io/gnatsd v1.4.1
	github.com/nats-io/nats-server v1.4.1
	github.com/nats-io/nats.go v1.13.1-0.20211122170419-d7c1d78a50fc
	github.com/phayes/freeport v0.0.0-20180830031419-95f893ade6f2
	github.com/rs/zerolog v1.26.1
	gitlab.alm.poste.it/go/ilog/ilog v0.1.2
	gitlab.alm.poste.it/go/ilog/zerolog v0.1.3
	gitlab.com/sbongini-go/mapstructureplus v0.0.0-20211221083647-be241237238d
	gitlab.com/sbongini-go/ratatoskr/core v0.0.0-20211222090201-09d9e7320f83
	gitlab.com/sbongini-go/ratatoskr/source/natssource v0.0.0-20211222093803-46fd66fcb16a
	go.opentelemetry.io/otel v1.3.0
	go.opentelemetry.io/otel/exporters/jaeger v1.3.0
	go.opentelemetry.io/otel/exporters/stdout/stdouttrace v1.3.0
	go.opentelemetry.io/otel/trace v1.3.0
	gopkg.in/yaml.v2 v2.4.0
	gotest.tools v2.2.0+incompatible
)

require (
	github.com/beorn7/perks v1.0.1 // indirect
	github.com/cespare/xxhash/v2 v2.1.2 // indirect
	github.com/go-logr/logr v1.2.2 // indirect
	github.com/go-logr/stdr v1.2.2 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/google/go-cmp v0.5.6 // indirect
	github.com/icza/dyno v0.0.0-20210726202311-f1bafe5d9996 // indirect
	github.com/matttproud/golang_protobuf_extensions v1.0.1 // indirect
	github.com/nats-io/nkeys v0.3.0 // indirect
	github.com/nats-io/nuid v1.0.1 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/prometheus/client_golang v1.11.0 // indirect
	github.com/prometheus/client_model v0.2.0 // indirect
	github.com/prometheus/common v0.32.1 // indirect
	github.com/prometheus/procfs v0.7.3 // indirect
	gitlab.alm.poste.it/go/ilog/stdlog v0.1.2 // indirect
	go.opentelemetry.io/otel/sdk v1.3.0 // indirect
	golang.org/x/crypto v0.0.0-20211215165025-cf75a172585e // indirect
	golang.org/x/sys v0.0.0-20211216021012-1d35b9e2eb4e // indirect
	google.golang.org/protobuf v1.27.1 // indirect
)
